# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  max = arr.sort[-1]
  min = arr.sort[0]

  max - min
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  arr == arr.sort
end


# MEDIUM

# Write a method that returns the number of vowels in its argument

VOWELS = %(a e i o u)

def num_vowels(str)
  vowelCount = 0

  str.downcase.each_char {|c| vowelCount += 1 if VOWELS.include?(c)}

  vowelCount
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  str = str.split

  str.each_with_index do |word, idx|
    str[idx] = ""

    word.each_char do |ch|
      unless VOWELS.include?(ch.downcase)
        str[idx] += ch
      end
    end
  end

  str.join(' ')
end

# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  int.to_s.split('').sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  str = str.downcase.split('')

  str.length != str.uniq.length
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  arr = arr.join

  first = arr[0..2]
  middle = arr[3..5]
  last = arr[6..-1]

  "(#{first}) #{middle}-#{last}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  max = str.split(',').sort[-1].to_i
  min = str.split(',').sort[0].to_i

  max - min
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  offset = offset % arr.length
  nums_to_rotate = arr.take(offset)
  unchanged = arr.drop(offset)

  unchanged.concat(nums_to_rotate)
end

=begin
a = [ "a", "b", "c", "d" ]
a.rotate         #=> ["b", "c", "d", "a"] [bcd][a]
a.rotate(2)      #=> ["c", "d", "a", "b"] [cd][ab]
a.rotate(-3)     #=> ["b", "c", "d", "a"] [bcd][a]
=end
